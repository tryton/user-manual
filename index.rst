==================
Tryton User Manual
==================

Target audience: Users of the Tryton GUI client or the Tryton Web
Client.

This manual describes how to use the
Tryton GUI client and
the Tryton Web client.
It explains
how to login to the Tryton server,
the user interface elements and
the elements of the most important Tryton modules.


.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
