Tryton User Manual
==========================================================

Target audience:
Users of the Tryton GUI client or the Tryton Web Client.

This manual describes how to use the Tryton GUI client and the Tryton
Web client. It explains how to login to the Tryton server, the user
interface elements and the elements of the most important Tryton
modules.


Information for People Working on this Documentation
------------------------------------------------------


### How to Locally Test Your Writings

This file will NOT become part of the publish documentation (due to the
filename ending `.md`).

Before pushing your work to the remote git repo, you might want to test
your mark-up locally. This helps keeping the number of "fixup" commits
in the git repo small - and thus the git history more concise.


### Unix, GNU/Linux, BSD, etc.


Just install Python 3 and make.
Then run:

    make html
    xdg-open _build/html/index.html
    # or point your Browser to _build/html/index.html

This will (once) install a Python virtual environment with all dependencies
for running sphinx, and then build the documentation.


If, during a build of the HTML documents, Sphinx produces an error message
like this:

    Could not import extension … (exception: No module named …')

just run once to update the virtual environment::

    make dist-clean html



### Windows


Just install Python 3 and sphinx, according to
<https://www.sphinx-doc.org/en/latest/usage/installation.html#windows>.
Then run::

    make.bat

and point your Browser to `_build/html/index.html`
